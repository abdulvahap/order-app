package com.abdulvahap.vahap;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayMessage(getString(R.string.hello_world));

    }

    /**
     * Displays the message in the application
     * @param message is the message to display
     */
    public void displayMessage(String message) {
        TextView textView = findViewById(R.id.textView);
        textView.setText(message);
        textView.setTextColor(Color.RED);
        textView.setTextSize(20);
    }
}